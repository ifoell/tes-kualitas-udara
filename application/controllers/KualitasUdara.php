<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KualitasUdara extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('kualitasUdaraModel');
	}

	public function index()
	{
		$kumodel = new kualitasUdaraModel;
		$data['data']=$kumodel->get_kualitas();
		$this->load->view('template/v_head');
		$this->load->view('template/v_sidebar');
		$this->load->view('template/v_header');
		$this->load->view('kualitas/v_content', $data);
		$this->load->view('template/v_footer');
		$this->load->view('template/v_logout');
		$this->load->view('template/v_scripts');
	}

	public function add(){
		$this->load->view('template/v_head');
		$this->load->view('template/v_sidebar');
		$this->load->view('template/v_header');
		$this->load->view('kualitas/v_add');
		$this->load->view('template/v_footer');
		$this->load->view('template/v_logout');
		$this->load->view('template/v_scripts');
	}

	public function store(){
		$kumodel = new kualitasUdaraModel;
		$kumodel->insert_kualitas();
		redirect(base_url());
	}

	public function edit($id){
		$kumodel = $this->db->get_where('kualitas', array('id' => $id)) -> row();
		$this->load->view('template/v_head');
		$this->load->view('template/v_sidebar');
		$this->load->view('template/v_header');
		$this->load->view('kualitas/v_edit',array('kualitas'=>$kumodel));
		$this->load->view('template/v_footer');
		$this->load->view('template/v_logout');
		$this->load->view('template/v_scripts');
	}

	public function update($id){
		$kumodel = new kualitasUdaraModel;
		$kumodel->update_kualitas($id);
		redirect(base_url());
	}

	public function delete($id){
		$this->db->where('id', $id);
		$this->db->delete('kualitas');
		redirect(base_url());	
	}

}

/* End of file kualitasUdara.php */
/* Location: ./application/controllers/kualitasUdara.php */