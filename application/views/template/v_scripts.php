<!-- Bootstrap core JavaScript-->
  <script src="<?php echo(base_url()); ?>_res/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo(base_url()); ?>_res/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo(base_url()); ?>_res/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo(base_url()); ?>_res/js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="<?php echo(base_url()); ?>_res/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="<?php echo(base_url()); ?>_res/vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?php echo(base_url()); ?>_res/js/demo/datatables-demo.js"></script>
  <script src="<?php echo(base_url()); ?>_res/js/confirm-delete.js"></script>


  </body>

</html>