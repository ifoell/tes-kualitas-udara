        <div class="container-fluid">
          <!-- Topbar -->
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Tabel Kualitas Udara</h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-body">
              
              <div class="float-right"><a href="<?php echo(base_url()); ?>kualitasudara/add" class="btn btn-primary" "><span class="fa fa-plus"></span> Add New</a></div>
              <br/><br/>
              
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Nama Kota</th>
                      <th>Kualitas</th>
                      <th>Tingkat Polusi</th>
                      <th>Suhu</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1; ?>
                    <?php foreach ($data as $d) { ?>      
                    <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $d->nama_kota; ?></td>
                        <?php if ($d->kualitas == 's') {
                          $d->kualitas = 'Sehat';
                        } else {
                          $d->kualitas = 'Tidak Sehat';
                        }
                        ?>
                        <td><?php echo $d->kualitas; ?></td>
                        <td><?php echo $d->tingkat_polusi; ?></td>
                        <td><?php echo $d->suhu; ?></td>
                        <td>
                          <form method="DELETE" action="<?php echo base_url('kualitasudara/delete/'.$d->id);?>" class="confirmClick">
                             <a class="btn btn-info btn-xs" href="<?php echo base_url('edit/'.$d->id) ?>"><i class="fas fa-edit"></i></a>
                            <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                          </form>
                        </td>
                    </tr>
                    <?php $no++ ?>
                    <?php } ?>
                  </tbody>
                  
                </table>
              </div>
            </div>
          </div>
        </div>