        <div class="container-fluid">
          <!-- Topbar -->
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Tambah Data</h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-body col-md-9 align-self-center">
                <form method="post" action="<?php echo base_url('add');?>">
                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="nama_kota" class="col-md-6">Nama Kota</label>
                              <div class="col-md-12">
                                  <input type="text" name="nama_kota" class="form-control">
                              </div>
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="kualitas" class="col-md-6">Kualitas Udara</label>
                                <select id="kualitas" class="form-control" name="kualitas">
                                  <option selected>Choose...</option>
                                  <option value="s">Sehat</option>
                                  <option value="ts">Tidak Sehat</option>
                                </select>
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="tingkat_polusi" class="col-md-6">Tingkat Polusi</label>
                              <div class="col-md-12">
                                  <input type="text" name="tingkat_polusi" class="form-control">
                              </div>
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="suhu" class="col-md-6">Suhu</label>
                              <div class="col-md-12">
                                  <input type="text" name="suhu" class="form-control">
                              </div>
                          </div>
                      </div>
                      <div class="col-md-3">
                          <input type="submit" name="Simpan" class="btn btn-primary  btn-user btn-block">
                      </div>
                  </div>
                  
              </form>
            </div>
          </div>
        </div>