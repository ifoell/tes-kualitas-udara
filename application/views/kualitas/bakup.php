                  <tbody>
                    <?php $no = 1; ?>
                    <?php foreach ($data as $d) { ?>      
                    <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $d->nama_kota; ?></td>
                        <?php if ($d->kualitas == 's') {
                          $d->kualitas = 'Sehat';
                        } else {
                          $d->kualitas = 'Tidak Sehat';
                        }
                        ?>
                        <td><?php echo $d->kualitas; ?></td>
                        <td><?php echo $d->tingkat_polusi; ?></td>
                        <td><?php echo $d->suhu; ?></td>
                        <td>
                          <form method="DELETE" action="<?php echo base_url('delete/'.$d->id);?>">
                             <a class="btn btn-info btn-xs" href="<?php echo base_url('edit/'.$d->id) ?>"><i class="fas fa-edit"></i></a>
                            <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                          </form>
                        </td>
                    </tr>
                    <?php $no++ ?>
                    <?php } ?>
                  </tbody>