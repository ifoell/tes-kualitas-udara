<!DOCTYPE html>
<html lang="en">

	<head>
		<?php $this->view('template/v_head'); ?>
	</head>

	<body id="page-top">
 		<!-- Page Wrapper -->
		<div id="wrapper">
    	<!-- Sidebar -->
    	<?php $this->view('template/v_sidebar'); ?>

    	    <div id="content-wrapper" class="d-flex flex-column">

      			<!-- Main Content -->
      			<div id="content">
      				<?php $this->view('template/v_header'); ?>
      				<div class="container-fluid">
      					<?php $this->view($_content.'/v_content'); ?>
      				</div>
			        <!-- /.container-fluid -->
			    </div>
      			<!-- End of Main Content -->
      			<?php $this->view('template/v_footer'); ?>
      		</div>
    		<!-- End of Content Wrapper -->
    	</div>
  		<!-- End of Page Wrapper -->

  		<!-- Scroll to Top Button-->
  		<a class="scroll-to-top rounded" href="#page-top">
    		<i class="fas fa-angle-up"></i>
  		</a>

  		<?php $this->view('template/v_logout'); ?>
  		<?php $this->view('template/v_scripts'); ?>

  	</body>

</html>

