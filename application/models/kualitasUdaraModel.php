<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class kualitasUdaraModel extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        // $this->load->database();
    }

	public function get_kualitas(){
        if(!empty($this->input->get("search"))){
          $this->db->like('nama_kota', $this->input->get("search"));
          $this->db->or_like('kualitas', $this->input->get("search"));
          $this->db->or_like('tingkat_polusi', $this->input->get("search"));
          $this->db->or_like('suhu', $this->input->get("search"));
        }
        $query = $this->db->get("kualitas");
        return $query->result();
    }
    public function insert_kualitas()
    {    
        $data = array(
            'nama_kota' => $this->input->post('nama_kota'),
            'kualitas' => $this->input->post('kualitas'),
            'tingkat_polusi' => $this->input->post('tingkat_polusi'),
            'suhu' => $this->input->post('suhu')
        );
        return $this->db->insert('kualitas', $data);
    }
    public function update_kualitas($id) 
    {
        $data=array(
            'nama_kota' => $this->input->post('nama_kota'),
            'kualitas'=> $this->input->post('kualitas'),
            'tingkat_polusi' => $this->input->post('tingkat_polusi'),
            'suhu' => $this->input->post('suhu')
        );
        if($id==0){
            return $this->db->insert('kualitas',$data);
        }else{
            $this->db->where('id',$id);
            return $this->db->update('kualitas',$data);
        }        
    }

}

/* End of file kualitasUdaraModel.php */
/* Location: ./application/models/kualitasUdaraModel.php */